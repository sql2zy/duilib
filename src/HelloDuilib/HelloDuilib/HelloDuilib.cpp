// HelloDuilib.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "HelloDuilibWnd.h"
#include "HelloDuilib.h"


int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	CPaintManagerUI::SetInstance(hInstance);

	CHelloDuilibWnd* wnd = new CHelloDuilibWnd;
	wnd->Create(NULL, NULL, UI_WNDSTYLE_DIALOG, 0);
	wnd->CenterWindow();
	wnd->ShowWindow();

	CPaintManagerUI::MessageLoop();

	delete wnd;

	return 0;
}