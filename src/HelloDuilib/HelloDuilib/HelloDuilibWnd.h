#pragma once

class CHelloDuilibWnd :
	public WindowImplBase
{
public:
	CHelloDuilibWnd(void);
	~CHelloDuilibWnd(void);

protected:
	virtual CDuiString GetSkinFolder() { return _T ("skin"); };
	virtual CDuiString GetSkinFile()  { return _T ("HelloDuilib.xml"); };
	virtual LPCTSTR GetWindowClassName(void) const { return _T ("HelloDuilib_Wnd"); };
};
