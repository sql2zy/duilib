#include "StdAfx.h"
#include "NovaUpdaterWnd.h"
#include <Duilib\UIBase.h>

CNovaUpdaterWnd::CNovaUpdaterWnd(void)
{
}

CNovaUpdaterWnd::~CNovaUpdaterWnd(void)
{
}


void CNovaUpdaterWnd::Notify(TNotifyUI& msg)
{
	if (msg.sType == DUI_MSGTYPE_CLICK)
	{
		//�ر�
		if (msg.pSender->GetName() == _T("btn_close"))
		{
			PostQuitMessage(0);
		}
		//���
		else if (msg.pSender->GetName() == _T("btn_maxsize"))
		{
			PostMessage(WM_SYSCOMMAND,SC_MAXIMIZE,0);
		}
		else if (msg.pSender->GetName() == _T("btn_minsize"))
		{
			PostMessage(WM_SYSCOMMAND,SC_MINIMIZE,0);
		}
	}
	else if (msg.sType == _T("selectchanged"))
	{
		CDuiString    strName     = msg.pSender->GetName();
		CTabLayoutUI* pControl = static_cast<CTabLayoutUI*>(m_PaintManager.FindControl(_T("tabTest")));

		if(strName == _T("Option1"))
			pControl->SelectItem(0);
		else if(strName == _T("Option2"))
			pControl->SelectItem(1);
		else if(strName == _T("Option3"))
			pControl->SelectItem(2);
	}
	__super::Notify(msg);
}

void CNovaUpdaterWnd::InitWindow()
{
	/*
	CActiveXUI* pActiveXUI = static_cast<CActiveXUI*>(m_PaintManager.FindControl("ActiveXUIDemo1"));
	if (pActiveXUI)
	{
		IWebBrowser2* pWebBrowser = NULL;
		pActiveXUI->SetDelayCreate(false);
		pActiveXUI->CreateControl(CLSID_WebBrowser);

		pActiveXUI->GetControl(IID_IWebBrowser2,(void**)&pWebBrowser);
		if (pWebBrowser != NULL)
		{
			pWebBrowser->Navigate(L"http://www.baidu.com/",NULL,NULL,NULL,NULL);
			pWebBrowser->Release();
		}
	}

	CProgressUI* pro = static_cast<CProgressUI*>(m_PaintManager.FindControl(_T("Progress")));
	if(pro)
		pro->SetValue(80);

	CSliderUI* Slider = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("SliderDemo")));
	if(Slider)
		Slider->SetValue(80);


	CListUI* list = static_cast<CListUI*>(m_PaintManager.FindControl(_T("ListDemo")));
	CDuiString str = "";
	for (int i=0; i<10;i++)
	{
		CListTextElementUI* pListElement = new CListTextElementUI;
		pListElement->SetTag(i);
		list->Add(pListElement);

		str.Format(_T("%d"), i);
		pListElement->SetText(0, str);
		pListElement->SetText(1, _T("haha"));
	}
*/

}

CControlUI* CNovaUpdaterWnd::CreateControl(LPCTSTR pstrClass)
{
	
	if (_tcsicmp(pstrClass,_T("Nova_TopHead")) == 0)
	{
		CDialogBuilder builder;
		CControlUI* pUI = builder.Create(_T("Nova_TopHead.xml")); 
		return pUI;
	}
	else 	if (_tcsicmp(pstrClass,_T("Nova_LeftCom")) == 0)
	{
		CDialogBuilder builder;
		CControlUI* pUI = builder.Create(_T("Nova_LeftCom.xml")); 
		return pUI;
	}
	else 	if (_tcsicmp(pstrClass,_T("Nova_Bottom")) == 0)
	{
		CDialogBuilder builder;
		CControlUI* pUI = builder.Create(_T("Nova_Bottom.xml")); 
		return pUI;
	}
	
	return NULL;
}