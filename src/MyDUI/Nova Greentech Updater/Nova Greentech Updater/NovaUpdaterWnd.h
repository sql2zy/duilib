#pragma once

class CNovaUpdaterWnd :
	public WindowImplBase
{
public:
	CNovaUpdaterWnd(void);
	~CNovaUpdaterWnd(void);

protected:


	virtual CDuiString GetSkinFolder() { return _T ("skin"); };

	virtual CDuiString GetSkinFile()  { return _T ("NovaGreentechUpdater.xml"); };
	virtual LPCTSTR GetWindowClassName(void) const { return _T ("Nova Greentech Updater"); };

	//message handle
	void Notify(TNotifyUI& msg);

	void InitWindow();

	CControlUI* CreateControl(LPCTSTR pstrClass);
};
