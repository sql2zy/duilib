﻿#pragma once
//#pragma execution_character_set("utf-8")
#include <windows.h>
#include <tchar.h>

#include <ObjBase.h>
#include <Duilib/UIlib.h>
#include "Duilib/Core/UIManager.h"

using namespace DuiLib;

#ifdef _DEBUG
#pragma comment (lib, "DuiLib_d.lib")
#else
#pragma comment (lib, "DuiLib.lib")
#endif

class CDuiFrameWnd:public CWindowWnd, public INotifyUI
{
public:

	//
	virtual LPCTSTR GetWindowClassName() const { return _T("DUIMainFrame"); }

	//
	virtual void    Notify(TNotifyUI& msg) {
		if(msg.sType == _T("click"))
		{
			if (msg.pSender->GetName() == _T("btnHello"))
			{
				::MessageBox(NULL,"I am button",NULL,NULL);
			}
		}
	}

	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		LRESULT lRes = 0;

		if( uMsg == WM_CREATE ) 
		{

			m_PaintManager.Init(m_hWnd);
			CDialogBuilder builder;
			CControlUI* root = builder.Create(_T("NovaGreentechUpdater.xml"),(UINT)0,NULL,&m_PaintManager);
			ASSERT(root && "Failed to parse XML");

			m_PaintManager.AttachDialog(root);
			m_PaintManager.AddNotifier(this);
			return lRes;
		}
		else if (uMsg == WM_NCACTIVATE)
		{
			if (!::IsIconic(m_hWnd))
			{
				return (0 == wParam) ? TRUE:FALSE;
			}
		}
		else if (uMsg == WM_NCCALCSIZE)
		{
			return 0;
		}
		else if (uMsg == WM_NCPAINT)
		{
			return 0;
		}

		if( m_PaintManager.MessageHandler(uMsg, wParam, lParam, lRes) ) 
		{
			return lRes;
		}

		return __super::HandleMessage(uMsg, wParam, lParam);
	}

protected:
	CPaintManagerUI m_PaintManager;
};

int APIENTRY _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpCmdLine, int nCmdShow)
{
	
	CPaintManagerUI::SetInstance(hInstance);
	CPaintManagerUI::SetResourcePath("../../Debug/skin");

	CDuiFrameWnd duiFrame;
	duiFrame.Create(NULL, _T("DUIWnd"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	duiFrame.CenterWindow();
	duiFrame.ShowModal();
	
	return 0;
}