
#pragma once
//#include <Duilib/Utils/Utils.h>
//#include <Duilib/Utils/WinImplBase.h>


class CCMyDuiWnd :
	public WindowImplBase
{
public:
	CCMyDuiWnd(void);
	~CCMyDuiWnd(void);

protected:
	virtual CDuiString GetSkinFolder() { return _T ("skin"); };

	virtual CDuiString GetSkinFile()  { return _T (/*"HelloDuilib.xml"*/"XMP.xml"); };
	virtual LPCTSTR GetWindowClassName(void) const { return _T ("HelloDuilib_Wnd"); };

	//message handle
	void Notify(TNotifyUI& msg);

	void InitWindow();

	CControlUI* CreateControl(LPCTSTR pstrClass);
};
