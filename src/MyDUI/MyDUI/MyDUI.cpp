// MyDUI.cpp : 定义应用程序的入口点。
//

#include "stdafx.h"
#include "MyDUI.h"
#include "CMyDuiWnd.h"
//#include <Duilib/core/UIManager.h>

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{

	CPaintManagerUI::SetInstance(hInstance);// 加载XML的时候，需要使用该句柄去定位EXE的路径，才能加载XML的路径

	CCMyDuiWnd* wnd = new CCMyDuiWnd; // 生成对象
	wnd->Create(NULL, NULL, UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE); // 创建DLG窗口
	wnd->CenterWindow(); // 窗口居中
	wnd->ShowWindow(); // 显示
	CPaintManagerUI::MessageLoop(); // 消息循环
	PostQuitMessage (0); 

	delete wnd; // 删除对象
	
	return 1;
}