#include "StdAfx.h"
#include "CMyDuiWnd.h"
#include "Duilib\UIActiveX.h"
#include "Duilib\Control\UIProgress.h"
#include "Duilib\Control\UIList.h"
#include "Duilib\Utils\Utils.h"
#include "Duilib\Layout\UITabLayout.h"

CCMyDuiWnd::CCMyDuiWnd(void)
{
}

CCMyDuiWnd::~CCMyDuiWnd(void)
{
}


void CCMyDuiWnd::Notify(TNotifyUI& msg)
{
	if (msg.sType == DUI_MSGTYPE_CLICK)
	{
		CDuiString d = msg.pSender->GetName();
		if (msg.pSender->GetName() == _T("closebtn"))
		{
			PostQuitMessage(0);
		}

		else if (msg.pSender->GetName() == _T("btn_close"))
		{
			PostQuitMessage(0);
		}
	}
	else if (msg.sType == _T("selectchanged"))
	{
		CDuiString    strName     = msg.pSender->GetName();
		CTabLayoutUI* pControl = static_cast<CTabLayoutUI*>(m_PaintManager.FindControl(_T("tabTest")));

		if(strName == _T("Option1"))
			pControl->SelectItem(0);
		else if(strName == _T("Option2"))
			pControl->SelectItem(1);
		else if(strName == _T("Option3"))
			pControl->SelectItem(2);
	}
	__super::Notify(msg);
}

void CCMyDuiWnd::InitWindow()
{
	/*
	CActiveXUI* pActiveXUI = static_cast<CActiveXUI*>(m_PaintManager.FindControl("ActiveXUIDemo1"));
	if (pActiveXUI)
	{
		IWebBrowser2* pWebBrowser = NULL;
		pActiveXUI->SetDelayCreate(false);
		pActiveXUI->CreateControl(CLSID_WebBrowser);

		pActiveXUI->GetControl(IID_IWebBrowser2,(void**)&pWebBrowser);
		if (pWebBrowser != NULL)
		{
			pWebBrowser->Navigate(L"http://www.baidu.com/",NULL,NULL,NULL,NULL);
			pWebBrowser->Release();
		}
	}

	CProgressUI* pro = static_cast<CProgressUI*>(m_PaintManager.FindControl(_T("Progress")));
	if(pro)
		pro->SetValue(80);

	CSliderUI* Slider = static_cast<CSliderUI*>(m_PaintManager.FindControl(_T("SliderDemo")));
	if(Slider)
		Slider->SetValue(80);


	CListUI* list = static_cast<CListUI*>(m_PaintManager.FindControl(_T("ListDemo")));
	CDuiString str = "";
	for (int i=0; i<10;i++)
	{
		CListTextElementUI* pListElement = new CListTextElementUI;
		pListElement->SetTag(i);
		list->Add(pListElement);

		str.Format(_T("%d"), i);
		pListElement->SetText(0, str);
		pListElement->SetText(1, _T("haha"));
	}
*/

}

CControlUI* CCMyDuiWnd::CreateControl(LPCTSTR pstrClass)
{
	
	if (_tcsicmp(pstrClass,_T("WndPlayPanel")) == 0)
	{
		CDialogBuilder builder;
		CControlUI* pUI = builder.Create(_T("WndPlayPanel.xml")); 
		return pUI;
	}
	else 	if (_tcsicmp(pstrClass,_T("Caption")) == 0)
	{
		CDialogBuilder builder;
		CControlUI* pUI = builder.Create(_T("caption.xml")); 
		return pUI;
	}
	
	return NULL;
}